Django Agencia Conocida
=======================

Our best brew of code utils for various types of django projects.
Cheers.

*Developers from [@AgenciaConocida](https://twitter.com/agenciaconocida)*


Installation
------------

    pip install -e git+https://bitbucket.org/interaliadesarrolloweb/django-agenciaconocida.git#egg=agenciaconocida

Setup
-----

    INSTALLED_APPS = (
        # ...,
        'agenciaconocida',
    )


Documentation
-------------

I know, I know. Finally!

[http://agencia-conocida.readthedocs.org/](http://agencia-conocida.readthedocs.org/)
