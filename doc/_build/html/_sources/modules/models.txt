Models
======

Abstract Models
---------------

.. autoclass:: agenciaconocida.models.Dated

.. autoclass:: agenciaconocida.models.SoftDelete
    :members: delete


Managers
--------

.. autoclass:: agenciaconocida.models.SoftDeleteManager
    :members:

Factories
---------

.. autofunction:: agenciaconocida.models.upload_handler_factory

Global Settings:

        **UPLOADS_PATH**

        *Default: 'uploads' (str)*

        Defines the relative path to append to MEDIA_ROOT and store uploaded files.

    Example::

        from django.db import models

        from agenciaconocida.models import upload_handler_factory


        class MyModel(models.Model):
            ...
            image = models.FileField(
                upload_to=upload_handler_factory('images'))
            ...