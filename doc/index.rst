.. agenciaconocida documentation master file, created by
   sphinx-quickstart on Tue Jan 21 16:59:40 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to agenciaconocida's documentation!
===========================================

Our best brew of code utils for various types of django projects.
Cheers.

*Developers from* `@AgenciaConocida <https://twitter.com/agenciaconocida>`_


Contents:

.. toctree::
   :maxdepth: 2

   modules/models
   modules/forms
   modules/views
   modules/context_processors
   modules/admin
   modules/utils


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

