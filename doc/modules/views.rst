Views
=====

Mixins
------

.. autoclass:: agenciaconocida.views.mixins.JsonMixin
    :members:

.. autoclass:: agenciaconocida.views.mixins.PDFMixin
    :members:

Throttle
--------

.. automodule:: agenciaconocida.views.throttle
    :members:


Fandjango Mixins
----------------

.. automodule:: agenciaconocida.views.fandjango
    :members: