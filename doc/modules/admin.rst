Admin
=====

.. automodule:: agenciaconocida.admin
    :members:

#TODO: There are several mokey patches for django admin in thins module that need to be documented.

Actions
-------

.. autofunction:: agenciaconocida.admin.actions.export_as_csv

Usage Example::

    class MyModelAdmin(admin.ModelAdmin):
        ...
        actions = [export_as_csv]
        ...