Forms
=====

Fields
------

.. automodule:: agenciaconocida.forms.fields
    :members:

Utils
-----

.. autofunction:: agenciaconocida.forms.utils.uploaded_file_clean_factory

Usage Example::

    from django import forms

    from agenciaconocida.forms.utils import uploaded_file_clean_factory


    class MyForm(forms.Form):
        ...
        image = forms.FileField()
        ...

        clean_image = uploaded_file_clean_factory(
            field_name='image',
            max_size=3*(1024**2), #3Mb
            allowed_extensions=('jpg', 'jpeg',),
            allowed_mimetypes=(
                'image/jpeg',
                'image/jpg',
                'application/octet-stream',
            )
        )