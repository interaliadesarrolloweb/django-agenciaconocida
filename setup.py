from setuptools import setup, find_packages

setup(
    version='0.1',
    description='django-agenciaconocida',
    long_description=open('README.md').read(),
    author='Desarrollo Web',
    author_email='desarrolloweb@interalia.net',
    name='agenciaconocida',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'django',
    ],
    license="GPL",
)
