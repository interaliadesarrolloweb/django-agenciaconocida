from django import forms


class HoneypotWidget(forms.TextInput):

    """
    Honeypot widget which hides the input using `display:none;`.
    """
    is_hidden = True

    def __init__(self, attrs=None, html_comment=False, *args, **kwargs):
        self.html_comment = html_comment
        super(HoneypotWidget, self).__init__(attrs, *args, **kwargs)
        if not 'class' in self.attrs:
            self.attrs['style'] = 'display:none'

    def render(self, *args, **kwargs):
        value = super(HoneypotWidget, self).render(*args, **kwargs)
        if self.html_comment:
            value = '<!-- %s -->' % value
        return value


class HoneypotField(forms.Field):

    """
    Honeypot field for forms which fails if value changes from the initial.

    Taken From: http://djangosnippets.org/snippets/131/

    In the following example, only the email field will be visible (the
    HoneypotFields are named as such to increase the chance that a bot
    will try to fill them in)::

        class EmailForm(Form):
           name = HoneypotField()
           website = HoneypotField(initial='leave me')
           email = EmailField()
    """
    widget = HoneypotWidget
    EMPTY_VALUES = (None, '')

    def clean(self, value):
        if self.initial in self.EMPTY_VALUES and value in self.EMPTY_VALUES or\
                value == self.initial:
            return value
        raise forms.ValidationError('Anti-spam field changed in value.')
