# coding:utf-8
from django import forms
from django.conf import settings


def uploaded_file_clean_factory(
        field_name, max_size=None, allowed_extensions=None,
        allowed_mimetypes=None):
    """
    To be added to a form wich validates file uploads.

    :param field_name: field name to be validated.
    :type field_name: str

    :param max_size: size in bytes. Default: UPLOAD_MAX_SIZE (1Mb)
    :type max_size: int

    :param allowed_extensions: list of allowed file extensions. Default: UPLOAD_ALLOWED_EXTENSIONS
    :type allowed_extensions: list or tuple

    :param allowed_mimetypes: list of allowed file mime types. Default: UPLOAD_ALLOWED_MIMETYPES
    :type allowed_mimetypes: list or tuple
    """
    if max_size is None:
        max_size = getattr(settings, 'UPLOAD_MAX_SIZE', 1048576)
    if allowed_extensions is None:
        allowed_extensions = getattr(settings, 'UPLOAD_ALLOWED_EXTENSIONS', (
            'jpg',
            'png',
            'gif',
        ))
    if allowed_mimetypes is None:
        allowed_mimetypes = getattr(settings, 'UPLOAD_ALLOWED_MIMETYPES', (
            'image/jpeg',
            'image/jpg',
            'image/png',
            'image/gif',
            'application/octet-stream',
        ))

    def clean_uploaded_file(self):
        uploaded_file = self.cleaned_data.get(field_name, None)
        if uploaded_file:
            # img_width, img_height = get_image_dimensions(uploaded_file)
            if hasattr(uploaded_file, "content_type") and \
                    uploaded_file.content_type not in allowed_mimetypes:
                msg = u"Este tipo de archivo es inválido. (%s)"
                raise forms.ValidationError(
                    msg % uploaded_file.content_type)

            if uploaded_file.size == 0:
                raise forms.ValidationError(u"El fichero enviado está vacío.")

            if uploaded_file.size > max_size:
                msg = u"El tamaño del archivo excede el máximo permitido!" + \
                    " (%d Mb)"
                raise forms.ValidationError(msg % (max_size / 1024 ** 2))
            extension = uploaded_file.name.split('.')[-1].lower()
            if extension not in allowed_extensions:
                msg = u"Este tipo de archivo es inválido. " + \
                    u"Verifica que la extensión esté permitida. (%s)"
                raise forms.ValidationError(msg % (
                    ", ".join(allowed_extensions)))
            # elif img_width != BANNER_WIDTH or img_height != BANNER_HEIGHT:
            #    msg = u"El archivo no cumple con las dimensiones " + \
            #        "establecidas: %d x %d."
            #    raise forms.ValidationError(msg % (
            #        BANNER_WIDTH, BANNER_HEIGHT))
        return uploaded_file
    return clean_uploaded_file
