# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.utils.decorators import method_decorator
try:
    from fandjango.decorators import (facebook_authorization_required,
                                      facebook_like_required)
    from django.settings import FACEBOOK_AUTHORIZATION_REDIRECT_URI
except ImportError:
    method_decorator = lambda x: x
    facebook_authorization_required = lambda x: lambda y: y
    facebook_like_required = lambda template: lambda x: x
    FACEBOOK_AUTHORIZATION_REDIRECT_URI = ""


class FacebookLikeRequiredMixin(object):

    """
    Adds fandjango's facebook_like_required decorator to view.
    """

    @method_decorator(facebook_like_required(template='like.html'))
    def dispatch(self, *args, **kwargs):
        return super(FacebookLikeRequiredMixin, self).dispatch(*args, **kwargs)


class FacebookAuthRequiredMixin(object):

    """
    Adds fandjango's facebook_authorization_required decorator to view.
    """

    @method_decorator(facebook_authorization_required(FACEBOOK_AUTHORIZATION_REDIRECT_URI))
    def dispatch(self, *args, **kwargs):
        return super(FacebookAuthRequiredMixin, self).dispatch(*args, **kwargs)


class FacebookAuthAndLikeRequiredMixin(object):

    """
    Adds both facebook_authorization_required and facebook_like_required decorator to view.
    """

    @method_decorator(facebook_authorization_required(FACEBOOK_AUTHORIZATION_REDIRECT_URI))
    @method_decorator(facebook_like_required(template='like.html'))
    def dispatch(self, *args, **kwargs):
        return super(FacebookAuthAndLikeRequiredMixin, self).dispatch(*args, **kwargs)
