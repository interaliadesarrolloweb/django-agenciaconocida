from django.contrib import messages
from django.shortcuts import render


def csrf_failure(request, reason=""):
    """
    A simple view for redirect the failed to a message
    
        CSRF_FAILURE_VIEW = "agenciaconocida.views.csrf_failure"

    """
    return render(request, template_name="agenciaconocida/csrf_failure.html",status="403")
