# -*- coding: utf-8 -*-
import os
import tempfile
from subprocess import Popen, PIPE

from django.conf import settings
from django.core import serializers
from django.http import HttpResponse
try:
    from django.utils import simplejson as json
except ImportError:
    import json

WKHTMLTOPDF_PATH = getattr(settings, 'WKHTMLTOPDF_PATH', "wkhtmltopdf")


class PDFMixin(object):

    """
    Generate PDF from view output using wkhtmltopdf
    """
    output_filename = 'output.pdf'

    def get_output_filename(self):
        """
        Gets the output filename. defaults to `output_filename`.
        """
        return self.output_filename

    def dispatch(self, request, *args, **kwargs):
        """
        Gets response content and renders it to PDF using WKHTMLTOPDF_PATH
        binary.
        """
        html_response = super(PDFMixin, self).dispatch(
            request, *args, **kwargs)
        html_file = tempfile.NamedTemporaryFile(suffix='.html', delete=False)
        try:
            html_file.write(
                html_response.rendered_content.encode("latin-1", "replace"))
            html_file.close()
            proc = Popen(
                [WKHTMLTOPDF_PATH, '-q', html_file.name, '-'],
                stdout=PIPE, stderr=PIPE)
            data = proc.stdout.read()
            response = HttpResponse(
                data, content_type='application/pdf')
            response['Content-Disposition'] = 'attachment; filename="%s"' % \
                self.get_output_filename()
        finally:
            os.remove(html_file.name)

        return response


class JsonMixin(object):

    """
    Render context as json and set the response's content_type.
    """

    def render_json_response(self, context):
        """
        Renders context as json using simplejson, returns `HttpResponse`
        with `content_type` to 'application/json'.

        :param context: Context data to be rendered as json.
        :type context: mixed

        :returns: HttpResponse
        """
        json_context = json.dumps(context)
        return HttpResponse(json_context, content_type="application/json")

    def serialize_json(self, context):
        """
        Renders context as json using django serializers, returns `HttpResponse`
        with `content_type` to 'application/json'.

        :param context: Context data to be rendered as json.
        :type context: mixed

        :returns: HttpResponse
        """
        data = serializers.serialize("json", context)
        return HttpResponse(data, content_type="application/json")
