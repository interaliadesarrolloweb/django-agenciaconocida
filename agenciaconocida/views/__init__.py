# -*- coding: utf-8 -*-
from .mixins import PDFMixin, JsonMixin
# backwards compatibility
GeneratePDFMixin = PDFMixin

from .fandjango import FacebookLikeRequiredMixin, FacebookAuthRequiredMixin, \
    FacebookAuthAndLikeRequiredMixin

from .throttle import throttle, uthrottle
from .friendly import csrf_failure
