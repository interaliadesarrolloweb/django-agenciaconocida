# TODO: reorganize this to diferent modules like models/uploads.py
import hashlib
from random import random
from os import path
from random import random

from django.conf import settings
from django.utils.encoding import smart_str

UPLOADS_PATH = getattr(settings, 'UPLOADS_PATH', 'uploads')


def upload_handler_factory(
        dir_name, uploads_path=UPLOADS_PATH, prefix='', suffix=''):
    """
    Generates method to handle the upload_to property of ``FileField``
    and ``ImageField``. Automaticaly generates unique hashed filenames to avoid
    filename conflicts while keeping file extension.

    :param dir_name: Subpath to be appended to ``uploads_path`` to store files.
    :type dir_name: str

    :param uploads_path: Allows to override the base uploads path. Default: ``UPLOADS_PATH``
    :type uploads_path: str

    :param prefix: String to prepend to the filename hash string. Default: ''.
    :type prefix: str

    :param suffix: String to append to the filename hash string. Default ''.
    :type suffix: str

    :returns: function -- upload_to handler

    """
    def upload_handler(instance, filename):
        new_filename = "%s%s%s.%s" % (
            prefix,
            hashlib.sha1("%s-%f" % (
                smart_str(filename), random())).hexdigest(),
            suffix,
            filename.split('.')[-1],
        )
        upload_path = path.join(uploads_path, dir_name, new_filename)
        return upload_path
    return upload_handler
