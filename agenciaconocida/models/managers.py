# -*- coding: utf-8 -*-
from django.db import models


class SoftDeleteManager(models.Manager):

    """
    Manager to filter out soft-deleted records.
    """

    def get_query_set(self):
        """
        Filters out all soft-deleted records from base QuerySet.

        :returns: QuerySet excluding soft-deleted elements.
        :rtype: QuerySet
        """
        query_set = super(SoftDeleteManager, self).get_query_set()
        return query_set.filter(deleted_at__isnull=True)
