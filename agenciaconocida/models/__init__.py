# -*- coding: utf-8 -*-
from .abstract import Dated, SoftDelete
from .managers import SoftDeleteManager
from .uploads import upload_handler_factory

# activate monkey patches
from agenciaconocida.monkeypatches import apply_monkey_patches
apply_monkey_patches()
