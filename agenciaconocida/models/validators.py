# encoding=utf-8

from django.core.validators import RegexValidator
#from django.utils.translation import ugettext as _
_ = lambda x:x
import re

REGEX_NAME = re.compile(ur'^[a-zA-Záéíóúñ]+\S$', re.UNICODE)
MSG_NAME = _(u"No aceptan caracteres extraños, números, espacios.")
name_validator = RegexValidator(regex=REGEX_NAME, message=MSG_NAME)



if __name__ == "__main__":
    def t(v):
        try:
            name_validator(v)
        except:
            pass


    name_validator('Andrés')
    name_validator('Andrésñamyuii')
    t('Andrés123123')
    t('Andrés&&')
    name_validator("áááááááééwrásdasñ")
    
