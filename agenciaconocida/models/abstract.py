# -*- coding: utf-8 -*-
import datetime
from random import random
from django.db import models, router
from django.db.models.deletion import Collector


from .managers import SoftDeleteManager


class Dated(models.Model):

    """
    Abstract model to add created_at and updated_at timestamps.
    """
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        abstract = True


class SoftDelete(models.Model):

    """
    Abstract model to override the delete method to soft-delete instances.
    """
    deleted_at = models.DateTimeField(blank=True, null=True)

    deleted_flag = models.BooleanField(verbose_name='Borrado', default=False)

    all_objects = models.Manager()
    objects = SoftDeleteManager()

    class Meta:
        abstract = True

    def delete(self, using=None):
        """
        Overrides default delete method and sets deletion timestamps to
        soft delete the instance.

        :param using: Database or router to use
        """
        hoy = datetime.datetime.now()
        self.deleted_at = hoy
        self.deleted_flag = True
        self.save()
        using = using or router.db_for_write(self.__class__, instance=self)

        err_msg = ("%s object can't be deleted because its " +
                   "%s attribute is set to None.") % (
                       self._meta.object_name, self._meta.pk.attname
                   )
        assert self._get_pk_val() is not None, err_msg

        seen_objs = Collector(using=using)
        seen_objs.collect([self])
        seen_objs.sort()
        # related_models = seen_objs.data.keys()

        for model, instances in seen_objs.data.iteritems():
            for ins in instances:
                if ins != self and ins.deleted_at is None:
                    ins.delete()
