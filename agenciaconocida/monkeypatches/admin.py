# -*- coding: utf-8 -*-
from django import forms
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.admin.models import LogEntry
from django.contrib.auth.forms import PasswordChangeForm
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

# Refer to the [docs](https://docs.djangoproject.com/en/1.9/topics/auth/customizing/#referencing-the-user-model)
User = settings.AUTH_USER_MODEL


def exists_instance(self):
    try:
        self.content_type.get_object_for_this_type(pk=self.object_id)
        return True
    except ObjectDoesNotExist:
        return False

LogEntry.exists_instance = exists_instance


PasswordChangeForm.error_messages = dict(PasswordChangeForm.error_messages, **{
    'password_match_old': _('Your new password matches with the old one. '
                            'Please enter it again.'),
})


def clean_old_password(self):
    old_password = self.cleaned_data['old_password']
    if not self.user.check_password(old_password):
        raise forms.ValidationError(
            self.error_messages['password_incorrect'])
    new_password1 = self.data['new_password1']
    user = User.objects.get(username=self.user.username)
    if user.check_password(new_password1):
        raise forms.ValidationError(
            self.error_messages['password_match_old'])
    return old_password

PasswordChangeForm.clean_old_password = clean_old_password
