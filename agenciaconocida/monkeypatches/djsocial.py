# -*- coding: utf-8 -*-
"""
Monkey patch python social auth
"""
from django.core import signing
from django.contrib.sessions.models import Session
from django.conf import settings
from social.exceptions import InvalidEmail


def custom_partial_pipeline_data(backend, user=None, *args, **kwargs):
    """
    Monkey patch utils.partial_pipeline_data to enable us to retrieve session
    data by signature key in request. This is necessary to allow users to
    follow a link in an email to validate their account (i.e. from cordova).

    We fetch only the needed details to complete the pipeline authorization
    process from the session.
    """
    # begin patch
    print '=> RECREATING AMBASSADOR SESSION =>'
    data = backend.strategy.request_data()
    if 'signature' in data:
        try:
            signed_details = signing.loads(data['signature'],
                                           key=settings.EMAIL_SECRET_KEY)
            session = Session.objects.get(pk=signed_details['session_key'])
        except signing.BadSignature, Session.DoesNotExist:
            raise InvalidEmail(backend)

        session_details = session.get_decoded()
        backend.strategy.session_set(
            'email_validation_address',
            session_details['email_validation_address'])
        backend.strategy.session_set('next', session_details.get('next'))
        backend.strategy.session_set('partial_pipeline',
                                     session_details.get('partial_pipeline'))
        session_name = backend.name + '_state'
        backend.strategy.session_set(session_name,
                                     session_details.get(session_name))
        session_unauthorized = backend.name + 'unauthorized_token_name'
        backend.strategy.session_set(session_unauthorized,
                                     session_details.get(session_unauthorized))
    # End patch, and continue normal flow.

    partial = backend.strategy.session_get('partial_pipeline', None)
    if partial:
        idx, backend_name, xargs, xkwargs = \
            backend.strategy.partial_from_session(partial)
        if backend_name == backend.name:
            kwargs.setdefault('pipeline_index', idx)
            if user:  # don't update user if it's None
                kwargs.setdefault('user', user)
            kwargs.setdefault('request', backend.strategy.request_data())
            xkwargs.update(kwargs)
            return xargs, xkwargs
        else:
            backend.strategy.clean_partial_pipeline()

from social import utils
print '=> ORIGINAL', utils.partial_pipeline_data
utils.partial_pipeline_data = custom_partial_pipeline_data
print '=> PATCHED', utils.partial_pipeline_data
