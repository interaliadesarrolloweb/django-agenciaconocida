# -*- coding: utf-8 -*-
import logging

from django.conf import settings
from importlib import import_module

logger = logging.getLogger(__name__)

MONKEY_PATCHES = getattr(settings, 'MONKEY_PATCHES', (
    'admin',
    'csrf',
    'django_cms',
))

PATCHED = []

def apply_monkey_patches(patches=MONKEY_PATCHES):
    for patch in patches:
        if not patch in PATCHED:
            # print "== Applying: %s" % patch
            import_module(".%s" % patch, 'agenciaconocida.monkeypatches')
            PATCHED.append(patch)
            logger.info("== Monkey Patch applied: %s" % patch)
    logger.info("== Fly monkeys, fly!! ñ_ñ")
