# -*- coding: utf-8 -*-
from django import VERSION
from django.conf import settings
from django.middleware.csrf import CsrfViewMiddleware
from django.utils.cache import patch_vary_headers

CSRF_COOKIE_HTTPONLY = getattr(settings, "CSRF_COOKIE_HTTPONLY", False)

if float("%d.%d" % VERSION[:2]) < 1.6:
    def process_response(self, request, response):
        if getattr(response, 'csrf_processing_done', False):
            return response
        if request.META.get("CSRF_COOKIE") is None:
            return response
        if not request.META.get("CSRF_COOKIE_USED", False):
            return response
        response.set_cookie(settings.CSRF_COOKIE_NAME,
                            request.META["CSRF_COOKIE"],
                            max_age=60 * 60 * 24 * 7 * 52,
                            domain=settings.CSRF_COOKIE_DOMAIN,
                            path=settings.CSRF_COOKIE_PATH,
                            secure=settings.CSRF_COOKIE_SECURE,
                            httponly=CSRF_COOKIE_HTTPONLY
                            )
        patch_vary_headers(response, ('Cookie',))
        response.csrf_processing_done = True
        return response

    CsrfViewMiddleware.process_response = process_response
