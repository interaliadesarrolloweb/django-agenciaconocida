# -*- coding: utf-8 -*-
import urllib
import urlparse
from django.conf import settings
from django.core.urlresolvers import reverse
from django.middleware.locale import LocaleMiddleware
LANGUAGE_COOKIE_HTTPONLY = getattr(settings, "LANGUAGE_COOKIE_HTTPONLY", False)


if 'cms' in settings.INSTALLED_APPS and settings.USE_I18N:
    from cms.middleware.multilingual import MultilingualURLMiddleware
    from cms.middleware.multilingual import has_lang_prefix, patch_response

    def process_response(self, request, response):
        language = getattr(
            request, 'LANGUAGE_CODE', self.get_language_from_request(request))
        local_middleware = LocaleMiddleware()
        response = local_middleware.process_response(request, response)
        path = unicode(request.path)
        if not hasattr(request, 'session') and \
                request.COOKIES.get(settings.LANGUAGE_COOKIE_NAME) != language:
            response.set_cookie(
                settings.LANGUAGE_COOKIE_NAME, value=language,
                httponly=LANGUAGE_COOKIE_HTTPONLY)
        if (not path.startswith(settings.MEDIA_URL) and
                not path.startswith(settings.STATIC_URL) and
                not (getattr(settings, 'STATIC_URL', False) and
           path.startswith(settings.STATIC_URL)) and
                response.status_code == 200 and
                response.has_header('Content-Type') and
                response._headers['content-type'][1].split(';')[0] ==
                "text/html"):
            pages_root = urllib.unquote(reverse("pages-root"))
            try:
                decoded_response = response.content.decode('utf-8')
            except UnicodeDecodeError:
                decoded_response = response.content
            response.content = patch_response(
                decoded_response,
                pages_root,
                request.LANGUAGE_CODE
            )
        if response.status_code == 301 or response.status_code == 302:
            location = response['Location']
            if location.startswith('.'):
                location = urlparse.urljoin(request.path, location)
                response['Location'] = location
            if (not has_lang_prefix(location) and location.startswith("/") and
                    not location.startswith(settings.MEDIA_URL) and
                    not (getattr(settings, 'STATIC_URL', False) and
               location.startswith(settings.STATIC_URL))):
                response['Location'] = "/%s%s" % (language, location)
        if request.COOKIES.get('django_language') != language:
            response.set_cookie(
                settings.LANGUAGE_COOKIE_NAME, value=language,
                httponly=LANGUAGE_COOKIE_HTTPONLY)
        return response

    MultilingualURLMiddleware.process_response = process_response
