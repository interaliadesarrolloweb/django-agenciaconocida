# -*- coding: utf-8 -*-

import base64
import os
import mimetypes

from django.core.files.uploadedfile import SimpleUploadedFile

from PIL import Image
from tastypie.fields import FileField


class Base64FileField(FileField):
    """
    A django-tastypie field for handling file-uploads through raw post data.
    It uses base64 for en-/decoding the contents of the file.
    Usage:

    from agenciaconocida.api import Base64FileField


    class MyResource(ModelResource):
        file_field = Base64FileField("file_field")

        class Meta:
            queryset = ModelWithFileField.objects.all()

    In the case of multipart for submission, it would also pass the filename.
    By using a raw post data stream, we have to pass the filename within our
    file_field structure:

    file_field = {
        "filename": "myfile.png",
        "b64string": "longbas64encodedstring",
        "ContentType": "image/png" # on hydrate optional
    }

    Your file_field will by dehydrated in the above format if the return64
    keyword argument is set to True on the field, otherwise it will simply
    return the URL.
    """

    def __init__(self, *args, **kwargs):
        self.return64 = kwargs.pop('return64', False)
        self.ensure_image = kwargs.pop('ensure_image', False)
        self.resource_name = kwargs.pop('resource_name', None)
        super(Base64FileField, self).__init__(*args, **kwargs)

    def _url(self, obj):
        instance = getattr(obj, self.instance_name, None)
        try:
            url = getattr(instance, 'url', None)
        except ValueError:
            url = None
        return url

    def dehydrate(self, bundle, **kwargs):
        if not self.return64:
            return self._url(bundle.obj)
        else:
            if (self.instance_name not in bundle.data
                    and hasattr(bundle.obj, self.instance_name)):
                file_field = getattr(bundle.obj, self.instance_name)
                if file_field:
                    content_type, encoding = mimetypes.guess_type(
                        file_field.file.name)

                    b64 = open(
                        file_field.file.name, "rb").read().encode("base64")
                    ret = {"filename": os.path.basename(file_field.file.name),
                           "b64string": b64,
                           "content_type": (content_type or
                                           "application/octet-stream")}
                    return ret
            return None

    def hydrate(self, bundle):
        value = super(Base64FileField, self).hydrate(bundle)

        if value and isinstance(value, dict):
            uploaded_file = SimpleUploadedFile(value["filename"],
                                      base64.b64decode(value["b64string"]),
                                      value.get("content_type",
                                                "application/octet-stream"))
            if self.ensure_image:
                try:
                    trial_image = Image.open(uploaded_file.file)
                    # trial_image.verify()
                    return uploaded_file
                except:
                    message = 'The file uploaded is not a valid image'
                    bundle.errors['image'] = message
                    if self.resource_name:
                        bundle.errors[self.resource_name] = {'image': [message]}
                    return bundle
            else:
                return uploaded_file

        elif isinstance(value, basestring):
            if value == self._url(bundle.obj):
                return getattr(bundle.obj, self.instance_name).name
            return value.replace('/media/', '')

        return None
