# -*- coding: utf-8 -*-

from django.contrib.sites.shortcuts import get_current_site

from django.conf import settings
from django.utils.translation import get_language


def site(request):
    """
    Adds site related data to your context.

    Available Variables:

        * DOMAIN - deterimed using sites framework or request data.
        * SITE_NAME - Removed since SITE_NAME is accessible from SITE.name
        * current_language - active language returned by django's `get_language`.
        * SITE - sites framework `Site` instance or `RequestSite`

    :rtype: dict
    """
    current_site = get_current_site(request)

    use_https = request.is_secure()
    context = {
        'DOMAIN': "%s://%s" % (use_https and 'https' or 'http', current_site.domain),
        'current_language': get_language(),
        'SITE': current_site,
    }
    return context


def social_settings(request):
    """
    Gets Social settings from django conf and makes them available to context.

    Available Variables:

        * FACEBOOK_APPLICATION_ID
        * FACEBOOK_URL_TAB
        * FACEBOOK_APPLICATION_URL_TAB
        * TITLE_FACEBOOK_SHARE
        * FANPAGE_URL
        * TWITTER_VIA
        * URL_TWITTER_SHARE

    :rtype: dict
    """
    context = site(request)
    context.update({
        'FACEBOOK_APPLICATION_ID': getattr(settings, 'FACEBOOK_APPLICATION_ID', None),
        'FACEBOOK_URL_TAB': getattr(settings, 'FACEBOOK_URL_TAB', None),
        'FACEBOOK_APPLICATION_URL_TAB': getattr(settings, 'FACEBOOK_APPLICATION_URL_TAB', None),
        'TITLE_FACEBOOK_SHARE': getattr(settings, 'TITLE_FACEBOOK_SHARE', None),
        'FANPAGE_URL': getattr(settings, 'FANPAGE_URL', None),
        'TWITTER_VIA': getattr(settings, 'TWITTER_VIA', None),
        'URL_TWITTER_SHARE': getattr(settings, 'URL_TWITTER_SHARE', None),
    })
    return context
